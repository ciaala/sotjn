function Samurai(name) {
    var weapon = "katana";
    this.getWeapon = function () {
        return weapon;
    };
    this.getName = function () {
        return name;
    }
    this.message = name + " wielding a " + weapon;
    this.getMessage = function () {
        return this.message;
    }
}

var samurai = new Samurai("Hattori");
console.log("message: " + samurai.getMessage());

console.log("name: " + samurai.getName());

console.log("weapon: " + samurai.getWeapon());
